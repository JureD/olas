import numpy as np


class Photon:
    def __init__(self,
                 location,
                 frequency,
                 phase,
                 direction,
                 c):
        if isinstance(location, np.ndarray):
            self.location = location
        else:
            self.location = np.array(location, dtype=float)
        self.frequency = frequency
        self.phase = phase
        self.direction = direction
        self.c = c
        self.in_transmission_counter = 0

    def show(self, ax):
        ax.plot(self.location[0], self.location[1], '.', color='red')

    def update_c(self, c):
        self.c = c

    def time_step(self, dt):
        self.location[0] += np.cos(self.direction) * self.c * dt
        self.location[1] += np.sin(self.direction) * self.c * dt
        self.in_transmission_counter -= 1

    def reflect(self,
                reflection_normal,
                reflection_distance,
                reflection_point):
        if self.in_transmission_counter <= 0:
            self.direction = 2 * reflection_normal - self.direction - np.pi
            self.location[0] = reflection_point[0] + np.cos(self.direction) * abs(reflection_distance)
            self.location[1] = reflection_point[1] + np.sin(self.direction) * abs(reflection_distance)

