from .shape.circle_segment import CircleSegment
import random


class Mirror:

    def __init__(self, shape, reflectivity):
        self.shape = shape
        self.reflectivity = reflectivity

    def show(self, ax):
        display_type = '-' if self.reflectivity == 1 else '--'
        self.shape.show(ax, display_type)

    def check_reflection(self, photon, mirror_index):
        self.shape.check_reflection(photon=photon, reflectivity=self.reflectivity, mirror_index=mirror_index)


class CircularMirror(Mirror):

    def __init__(self,
                 reflectivity,
                 radius,
                 diameter,
                 translation,
                 rotation):
        shape = CircleSegment(radius=radius,
                              diameter=diameter,
                              translation=translation,
                              rotation=rotation)

        super().__init__(shape=shape,
                         reflectivity=reflectivity)
