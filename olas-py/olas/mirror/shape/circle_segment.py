import numpy as np
from numba import jit
import time
import random


@jit
def _check_reflection(photon_direction, photon_location, rotation_matrix, translation, rotation, diameter, radius,
                      reflectivity):
    photon_location = np.asarray(np.matmul(rotation_matrix.transpose(), photon_location))[0]
    photon_direction = photon_direction - rotation

    k = np.tan(photon_direction)
    n = photon_location[1] - k * photon_location[0]
    q = np.matmul(rotation_matrix.transpose(), (translation[0], 0))[0, 1]
    p = np.matmul(rotation_matrix.transpose(), (0, translation[1]))[0, 0]

    x = np.asarray(np.roots([1 + k ** 2,
                             -2 * (p + k * (q - n)),
                             n ** 2 + p ** 2 - 2 * n * q + q ** 2 - radius ** 2]))

    for x_i in x:
        if np.isreal(x_i):
            if -diameter / 2 <= x_i <= diameter / 2:
                if (-1e-3 < photon_direction < 1e-3) or ((np.pi - 1e-3) < photon_direction < (np.pi + 1e-3)):
                    y = np.asarray(np.roots([1, -2 * q, +x_i ** 2 - 2 * p * x_i + p ** 2 + q ** 2 - radius ** 2]))
                    y = y[0] if y[0] > 0 else y[1]
                else:
                    y = k * x_i + n

                if y > 0:
                    distance = np.matmul((x_i, y) - photon_location,
                                         [[np.cos(photon_direction)], [np.sin(photon_direction)]])
                    normal = np.pi + np.arctan2(y, x_i) + rotation
                    if (-1 < distance < 0) and (np.cos(photon_direction - normal) < 0):
                        reflection_point = np.asarray(np.matmul(rotation_matrix, np.array((x_i, y))))[0]
                        # if random.random() < reflectivity:
                        return normal, distance, reflection_point
                        # else:
                        #     return True


class CircleSegment:

    def __init__(self,
                 radius,
                 diameter,
                 translation,
                 rotation):
        self.radius = radius
        self.diameter = diameter
        self.translation = translation
        self.rotation = rotation

        self.rotation_matrix = np.matrix([[np.cos(rotation), -np.sin(rotation)],
                                          [np.sin(rotation), np.cos(rotation)]])

    def show(self, ax, display_type):
        x = np.arange(-round(self.diameter / 2), round(self.diameter / 2), step=1)
        y = np.sqrt(self.radius ** 2 - (x) ** 2)
        x += self.translation[1]
        y -= self.translation[0]
        xy = np.column_stack((x, y))
        for i in range(len(xy)):
            xy[i] = np.matmul(self.rotation_matrix, xy[i])
        ax.plot(xy[:, 0], xy[:, 1], display_type, color='black')

    def check_reflection(self, photon, reflectivity, mirror_index):
        photon_location = photon.location
        photon_direction = photon.direction
        result = _check_reflection(photon_direction=photon_direction,
                                   photon_location=photon_location,
                                   rotation_matrix=self.rotation_matrix,
                                   translation=self.translation,
                                   rotation=self.rotation,
                                   diameter=self.diameter,
                                   radius=self.radius,
                                   reflectivity=reflectivity)

        if len(result) > 1:
            photon.reflect(reflection_normal=result[0],
                           reflection_distance=result[1],
                           reflection_point=result[2])
