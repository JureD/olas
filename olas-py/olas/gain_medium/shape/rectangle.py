import numpy as np


class Rectangle:
    def __init__(self,
                 size,
                 translation,
                 rotation):
        self.size = size
        self.translation = translation
        self.rotation = rotation

        self.rotation_matrix = np.matrix([[np.cos(rotation), -np.sin(rotation)],
                                          [np.sin(rotation), np.cos(rotation)]])

    def show(self, ax):
        corner_points = np.array(((self.size[0] / 2 + self.translation[0], self.size[1] / 2 + self.translation[1]),
                                 (self.size[0] / 2 + self.translation[0], -self.size[1] / 2 + self.translation[1]),
                                 (-self.size[0] / 2 + self.translation[0], -self.size[1] / 2 + self.translation[1]),
                                 (-self.size[0] / 2 + self.translation[0], self.size[1] / 2 + self.translation[1])))

        for i in range(len(corner_points)):
            corner_points[i] = np.matmul(self.rotation_matrix, corner_points[i])

        ax.plot((corner_points[0][0], corner_points[1][0]),
                (corner_points[0][1], corner_points[1][1]),
                '-', color='gray')
        ax.plot((corner_points[1][0], corner_points[2][0]),
                (corner_points[1][1], corner_points[2][1]),
                '-', color='gray')
        ax.plot((corner_points[2][0], corner_points[3][0]),
                (corner_points[2][1], corner_points[3][1]),
                '-', color='gray')
        ax.plot((corner_points[3][0], corner_points[0][0]),
                (corner_points[3][1], corner_points[0][1]),
                '-', color='gray')
