import random
import numpy as np

class DV:
    def __init__(self,
                 center,
                 tau,
                 pump_rate,
                 transition_cross_section,
                 emission_frequency,
                 c,
                 units,
                 ):
        self.center = center
        self.tau = tau
        self.pump_rate = pump_rate
        self.transition_cross_section = transition_cross_section
        self.emission_frequency = emission_frequency
        self.n = c
        self.units = units
