class GainMedium:

    def __init__(self,
                 shape,
                 n,
                 tau,
                 pump_rate,
                 transition_cross_section,
                 emission_frequency):
        self.shape = shape
        self.units = None
        self.n = n
        self.tau = tau
        self. pump_rate = pump_rate
        self.transition_cross_section = transition_cross_section
        self.emission_frequency = emission_frequency
        self.c = 299792458/n

    def add_units(self, units):
        self.units = units

    def show(self, ax):
        self.shape.show(ax)
