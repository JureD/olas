from .mirror.mirror import Mirror
from .gain_medium.gain_medium import GainMedium


class Resonator:
    def __init__(self, n):
        self.mirrors = []
        self.gain_medium = []
        self.n = n

        self.c = 299792458/n

    def add_mirror(self, mirror):
        if isinstance(mirror, Mirror):
            self.mirrors.append(mirror)
        else:
            print('ERR: mirror should be of type Mirror')

    def add_mirrors(self, mirrors):
        for mirror in mirrors:
            self.add_mirror(mirror)

    def add_gain_medium(self, gain_medium):
        if isinstance(gain_medium, GainMedium):
            self.gain_medium.append(gain_medium)
        else:
            print('ERR: gain medium should be of type GainMedium')

    def show(self, ax):
        for mirror in self.mirrors:
            mirror.show(ax)
        for gain_medium in self.gain_medium:
            gain_medium.show(ax)

    def check_reflection(self, photon):
        for mirror_index, mirror in enumerate(self.mirrors):
            mirror.check_reflection(photon=photon, mirror_index=mirror_index)


