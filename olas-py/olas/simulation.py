import matplotlib.pyplot as plt
from multiprocessing import Pool

class Simulation:
    c = 299792458

    def __init__(self,
                 dimensions,
                 units):
        self.resonator = None
        self.dimensions = dimensions
        self.units = units

        self.dt = units / (3 * self.c)

        self.photons = []

        self.step_counter = 0
        self.time = 0

    def add_resonator(self, resonator):
        self.resonator = resonator

    def preview(self, **kwargs):
        ax = plt.axes()
        ax.set_aspect('equal')
        if 'resonator' in kwargs and kwargs['resonator'] is True:
            if self.resonator is not None:
                self.resonator.show(ax)
            else:
                print('ERR: can\'t preview resonator - not defined')
        if 'photon' in kwargs and kwargs['photon'] is True:
            for photon in self.photons:
                photon.show(ax)

    def add_photon(self, photon):
        self.photons.append(photon)

    def time_step(self):
        self.step_counter += 1
        self.time += self.dt

        def photon_time_step(p):
            p.time_step(dt=self.dt)

        def resonator_check_reflection(p):
            self.resonator.check_reflection(photon=p)

        # num_of_threads = 10
        # for i in range(int(len(lista) / num_of_threads) + 1):
        #     print(lista[(i) * 10:(i + 1) * 10])

        # with Pool(10) as pool:
        #     pool.map_async(photon_time_step, self.photons)

        for photon in self.photons:
            photon_time_step(photon)
        for photon in self.photons:
            resonator_check_reflection(photon)


    def time_steps(self, number_of_steps):
        for i in range(number_of_steps):
            self.time_step()
