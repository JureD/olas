import numpy as np
import olas

simulation = olas.Simulation((60, 30), 1e-3)

resonator = olas.Resonator(n=1)

hr_shape = olas.mirror.shape.CircleSegment(radius=30,
                                           diameter=30,
                                           translation=(0, 0),
                                           rotation=-np.pi/2)
hr = olas.mirror.Mirror(shape=hr_shape,
                        reflectivity=1)

oc_shape = olas.mirror.shape.CircleSegment(radius=30,
                                           diameter=30,
                                           translation=(0, 0),
                                           rotation=np.pi/2)
oc = olas.mirror.Mirror(shape=oc_shape,
                        reflectivity=0.5)
# hr = olas.mirror.CircularMirror(reflectivity=1,
#                                 radius=30,
#                                 diameter=20,
#                                 translation=(0, 0),
#                                 rotation=0)
# oc = olas.mirror.CircularMirror(reflectivity=0.5,
#                                 radius=30,
#                                 diameter=20,
#                                 translation=(0, 0),
#                                 rotation=0)
resonator.add_mirror(hr)
# resonator.add_mirrors((hr, oc))


# rectangle = olas.gain_medium.shape.Rectangle(size=(20, 10),
#                                              translation=(0, 0),
#                                              rotation=0)
#
# gain_medium = olas.gain_medium.GainMedium(shape=rectangle,
#                                           n=1.76,
#                                           tau=4.8e-9,
#                                           pump_rate=3,
#                                           transition_cross_section=2.5e-24,
#                                           emission_frequency=400e12)
#resonator.add_gain_medium(gain_medium)

simulation.add_resonator(resonator=resonator)

photon = olas.Photon(location=[-29.5, 0],
                     frequency=400e12,
                     phase=0,
                     direction=-np.pi,
                     c=299792458)
simulation.add_photon(photon=photon)
simulation.time_step()
#simulation.preview(resonator=True, photon=True)