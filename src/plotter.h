#ifndef PLOTTER_H
#define PLOTTER_H

#include <iostream>
#include <Magick++.h>
#include <string>
#include "photon.h"
#include "aux.h"

class Plotter {
public:
    Plotter(size_t x_dimension, size_t y_dimension);
    void draw_photon(unsigned long x, unsigned long y);
    void draw_gain_medium(unsigned long x, unsigned long y);
    void save(std::string filename);

private:
    unsigned long width;
    unsigned long height;
    Magick::Image *image;
    Magick::Pixels *view;
    Magick::Quantum *pixels;

    Magick::Color photon_color;
    Magick::Color gain_medium_color;


    void paint_pixel(unsigned int x, unsigned int y, Magick::Color color);
};

#endif // PLOTTER_H






