#ifndef RESONATOR_H
#define RESONATOR_H

#include <iostream>
#include <vector>
#include "aux.h"
#include "mirror.h"
#include "gain_medium.h"
#include "plotter.h"

class Resonator
{
    public:
        Resonator(double n);
        void add_mirror(Mirror *mirror);
        void add_gain_medium(GainMedium *gain_medium);
        void print();
        void preview(Plotter * plotter_);
        void time_step(double dt, std::list<Photon> *photons);

    private:
        double c;
        std::vector<Mirror *> mirrors;
        std::vector<GainMedium *> gain_mediums;

};

#endif // RESONATOR_H