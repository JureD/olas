#ifndef SIMULATION_H
#define SIMULATION_H

#include <cmath>
#include <list>
#include <iostream>

#include "aux.h"
#include "photon.h"
#include "resonator.h"
#include "plotter.h"


class Simulation
{
    public:
        Resonator *resonator;
        Simulation(unsigned long x_dimension,
                   unsigned long y_dimension,
                   int base_10_units,
                   unsigned int time_scaler_,
                   double n_);
        void preview();
        void print();
        void time_step();

    
    private:
        unsigned long x_dimension;
        unsigned long y_dimension;
        unsigned int time_scaler;
        double units;
        double dt;
        std::list<Photon> photons;

        void add_photon(Coordinates location,
                        double direction,
                        double phase,
                        double frequency);
};

#endif // SIMULATION_H