#include "simulation.h"

Simulation::Simulation(unsigned long x_size,
                       unsigned long y_size,
                       int base_10_units,
                       unsigned int time_scaler_,
                       double n_)
{
    x_dimension = x_size;
    y_dimension = y_size;
    time_scaler = time_scaler_;
    units = pow(10, base_10_units);
    resonator = new Resonator(n_);

    Coordinates loc;
    loc.x = 29;
    loc.y = 19;
    add_photon(loc,0,0,0);
    loc.x = 0;
    loc.y = 0;
    add_photon(loc,0,0,0);
    loc.x = 29;
    loc.y = 0;
    add_photon(loc,0,0,0);
    loc.x = 0;
    loc.y = 19;
    add_photon(loc,0,0,0);


    dt = units / (C_0 * time_scaler);
}

void Simulation::print()
{
    std::cout << "OLAS simulation\n" <<
                 "|-> X: " << x_dimension << '\n' <<
                 "|-> Y: " << y_dimension << '\n' <<
                 "|-> time scaler: " << time_scaler << '\n' <<
                 "|-> units: " << units << " m\n";
    resonator->print();
}

void Simulation::preview()
{
    Plotter plotter(x_dimension, y_dimension);

    if (!photons.empty()) {
        for (auto & photon : photons) {
            Coordinates coordinates = photon.get_location();
            plotter.draw_photon((int)round(coordinates.x), (int)round(coordinates.y));
        }
    }


    resonator->preview(&plotter);

    std::string filename = "/home/jure/Pictures/preview.png";
    plotter.save(filename);

}

void Simulation::add_photon(Coordinates location,
                            double direction,
                            double phase,
                            double frequency)
{
    Photon new_photon(location, direction, phase, frequency);
    photons.push_back(new_photon);
}

void Simulation::time_step() {
    resonator->time_step(dt, &photons);
}