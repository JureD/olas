#include "resonator.h"

Resonator::Resonator(double n)
{
    c = C_0/n;
}

void Resonator::print()
{
    std::cout << "|-> Resonator\n" <<
                 "    |-> Mirrors\n";

    if (mirrors.empty()) {
        for (unsigned char i = 0 ; i < mirrors.size() ; i++) {
            //mirrors[i].print();
        }
    } else {
        std::cout << "        |-x not defined\n";
    }

    std::cout << "    |-> Gain medium\n";

    if (gain_mediums.empty()) {
        for (unsigned char i = 0 ; i < gain_mediums.size() ; i++) {
            //gain_mediums[i].print();
        }
    } else {
        std::cout << "        |-x not defined\n";
    }
}

void Resonator::add_mirror(Mirror *mirror)
{
    mirrors.push_back(mirror);
}
void Resonator::add_gain_medium(GainMedium *gain_medium)
{
    gain_mediums.push_back(gain_medium);
}

void Resonator::preview(Plotter * plotter_) {
    if (!mirrors.empty()) {
        for (unsigned char i = 0 ; i < mirrors.size() ; i++) {
            //mirrors[i].preview(plotter_);
        }
    }

    if (!gain_mediums.empty()) {
        for (unsigned char i = 0 ; i < gain_mediums.size() ; i++) {
            gain_mediums[i]->preview(plotter_);
        }
    }
}

void Resonator::time_step(double dt, std::list<Photon> *photons_) {
    for (unsigned int i = 0 ; i < gain_mediums.size() ; i++) {
        gain_mediums[i]->time_step(dt, photons_);
    }
}