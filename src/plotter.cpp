#include "plotter.h"

Plotter::Plotter(unsigned long x_dimension,unsigned long y_dimension)
{
    width = x_dimension;
    height = y_dimension;

    Magick::InitializeMagick("");

    image = new Magick::Image(Magick::Geometry(width, height), "white");

    image->type(Magick::TrueColorType);
    image->modifyImage();

    view = new Magick::Pixels(*image);

    pixels = view->get(0, 0, width, height);

    photon_color = Magick::Color (1,0,0);
    gain_medium_color = Magick::Color (0.7,0.7,0.7);

}
void Plotter::draw_photon(unsigned long x, unsigned long y)
{
    paint_pixel(x, y, photon_color);

    view->sync();

}

void Plotter::draw_gain_medium(unsigned long x, unsigned long y)
{
    paint_pixel(x, y, gain_medium_color);

    view->sync();

}

void Plotter::paint_pixel(unsigned int x, unsigned int y, Magick::Color color)
{
    *(pixels+(y*width + x)*3) = QuantumRange * color.quantumRed();
    *(pixels+(y*width + x)*3+1) = QuantumRange * color.quantumGreen();
    *(pixels+(y*width + x)*3+2) = QuantumRange * color.quantumBlue();
}

void Plotter::save(std::string filename)
{
    image->write(filename);
}

//using namespace std;
//using namespace Magick;
//int main(int argc,char **argv) {
//    InitializeMagick(*argv);
//
//    // Create base image
//    Image image(Geometry(10, 10), "white");
//
//
//    // Set the image type to TrueColor DirectClass representation.
//    image.type(TrueColorType);
//    // Ensure that there is only one reference to underlying image
//    // If this is not done, then image pixels will not be modified.
//    image.modifyImage();
//
//    // Allocate pixel view
//    Pixels view(image);
//
//    // Set all pixels in region anchored at 38x36, with size 160x230 to green.
//    Color green(0,1,1);
//    Quantum *pixels = view.get(0, 0, 10, 10);
//    pixels += 3;
//    *pixels++ = QuantumRange * green.quantumRed();
//    *pixels++ = QuantumRange * green.quantumGreen();
//    *pixels = QuantumRange * green.quantumBlue();
//
//
//
//
//    // Save changes to image.
//    view.sync();
//    image.write( "output.png" );
//}