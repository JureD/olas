#include "dv.h"

Dv::Dv(GainMediumData *gain_medium_data_) {
    gain_medium_data = gain_medium_data_;
    N2ostanek =0;

    direction_distribution = std::uniform_real_distribution<double>(0, 2*M_PIf32);
    direction_generator = std::default_random_engine(direction_rd());

    phase_distribution = std::uniform_real_distribution<double>(0, 2*M_PIf32);
    phase_generator = std::default_random_engine(phase_rd());
}

void Dv::time_step(double dt, Coordinates coordinates_, std::vector<Photon> *photons_in_cell_, std::list<Photon> *photons_) {
    time_step_pump();
    time_step_spont_emiss(coordinates_, photons_);
}

void Dv::time_step_pump() {
    N2 += gain_medium_data->pump_rate;
}

void Dv::time_step_spont_emiss(Coordinates coordinates_, std::list<Photon> *photons_) {
    N2ostanek += (1/gain_medium_data->tau) * N2;
    auto emissed_photons_no = static_cast<unsigned int>(floor(N2ostanek));
    N2ostanek -= emissed_photons_no;

    for (unsigned int i = 0 ; i < emissed_photons_no ; i++) {
        Photon new_photon = Photon(coordinates_,
                                   direction_distribution(direction_generator),
                                   phase_distribution(phase_generator),
                                   gain_medium_data->emission_frequency);
        photons_->push_back(new_photon);
    }

}

void Dv::time_step_stimul_emiss(std::vector<Photon> *photons) {

}