


#include "photon.h"

Photon::Photon(Coordinates initial_location,
               double initial_direction,
               double initial_phase,
               double initial_frequency)
{
    location = initial_location;
    direction = initial_direction;
    phase = initial_phase;
    frequency = initial_frequency;
}


void Photon::update(double dt)
{
    
}


Coordinates Photon::get_location(void)
{
    return location;
}

void Photon::set_c(double n)
{
    c = C_0 / n;
}
