#include "gain_medium.h"


GainMedium::GainMedium(double n_,
                       double tau_,
                       unsigned int pump_rate_,
                       double transition_cross_section_,
                       double emission_frequency_) {
    n = n_;
    data.tau = tau_;
    data.pump_rate = pump_rate_;
    data.transition_cross_section = transition_cross_section_;
    data.emission_frequency = emission_frequency_;
}

void GainMedium::print() {
    std::cout << "    |-> Gain medium\n";
}

void GainMedium::set_units(double units_) {
    units = units_;
}


void GainMedium::set_gain_medium_data(GainMediumData gain_medium_data_) {
    data = gain_medium_data_;
}

void Rectangle::preview(Plotter *plotter_) {
    unsigned long x_0 = center_x - round(dimension_x / 2);
    unsigned long y_0 = center_y - round(dimension_y / 2);
    unsigned long y_1 = center_y + round(dimension_y / 2);
    for (unsigned long x = 0; x < dimension_x; x++) {
        plotter_->draw_gain_medium(x_0 + x, y_0);
        plotter_->draw_gain_medium(x_0 + x, y_1);
    }
}

Rectangle::Rectangle(GainMediumData gainm_edium_data_,
                     long center_x_,
                     long center_y_,
                     unsigned long dimension_x_,
                     unsigned long dimension_y_) {
    data = gainm_edium_data_;
    center_x = center_x_;
    center_y == center_y_;
    dimension_x = dimension_y_;
    dimension_y = dimension_y_;

}

Rectangle::Rectangle(double n_,
                     double tau_,
                     unsigned int pump_rate_,
                     double transition_cross_section_,
                     double emission_frequency_,
                     long center_x_,
                     long center_y_,
                     unsigned long dimension_x_,
                     unsigned long dimension_y_) {
    data.n = n_;
    data.tau = tau_;
    data.pump_rate = pump_rate_;
    data.transition_cross_section = transition_cross_section_;
    data.emission_frequency = emission_frequency_;
    center_x = center_x_;
    center_y == center_y_;
    dimension_x = dimension_y_;
    dimension_y = dimension_y_;
}

void Rectangle::make_mesh() {
    for (unsigned long y = 0 ; y < dimension_y ; y++) {
        std::vector<Dv*> current_line;
        for (unsigned long x ; x < dimension_x ; x++) {
            //auto new_photon =
            current_line.emplace_back(new Dv(&data));
        }
        mesh.push_back(current_line);
    }
}

int Rectangle::is_inside(Coordinates coordinates_) {
    unsigned long coordinate_x = static_cast<unsigned long>(coordinates_.x);
    unsigned long coordinate_y = static_cast<unsigned long>(coordinates_.y);
    if (coordinate_x < (center_x + dimension_x/2)) {
        if (coordinate_x > (center_x - dimension_x/2)) {
            if (coordinate_y < (center_y + dimension_y/2)) {
                if (coordinate_y > (center_y - dimension_y/2)) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}

void Rectangle::time_step(double dt, std::list<Photon> *photons_) {
    std::vector<std::vector<std::vector<Photon *>>> photon_mesh;
    for (unsigned long y = 0; y < mesh.size(); y++) {
        std::vector<std::vector<Photon *>> current_row;
        for (unsigned long x = 0; x < mesh[y].size(); x++) {
            std::vector<Photon *> current_cell;
            current_cell.push_back(nullptr);
            current_row.push_back(current_cell);
        }
        photon_mesh.push_back(current_row);
    }

    for (auto &photon: *photons_) {
        Coordinates coordinates = photon.get_location();
        long location_mesh_x = static_cast<long>(floor(coordinates.x));
        long location_mesh_y = static_cast<long>(floor(coordinates.y));

        if (!shape->is_not_inside(location_mesh_x,
                                  location_mesh_y)) {
            unsigned long mesh_location = shape->get_mesh_location(&photon);
            photon_mesh[]
        }
    }

    for (unsigned long y = 0; y < mesh.size(); y++) {
        for (unsigned x = 0; mesh[y].size(); x++) {

            mesh[y][x]->time_step(dt, Coordinates
            coordinates_, std::vector<Photon> * photons_in_cell_, photons_);
        }
    }
}