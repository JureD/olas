add_library(olas
        aux.cpp
        simulation.cpp
        resonator.cpp
        mirror.cpp
        photon.cpp
        plotter.cpp
        gain_medium.cpp
        dv.cpp
        gain_medium_shape.cpp
)

find_package(PkgConfig)
pkg_check_modules(GTKMM_VARS REQUIRED IMPORTED_TARGET gtkmm-4.0)
target_link_libraries(olas PkgConfig::GTKMM_VARS)

find_package(PkgConfig)
pkg_check_modules(MAGICK_VARS REQUIRED IMPORTED_TARGET Magick++)
target_link_libraries(olas PkgConfig::MAGICK_VARS)