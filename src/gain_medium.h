#ifndef GAIN_MEDIUM_H
#define GAIN_MEDIUM_H

#include <vector>
#include "aux.h"
#include "dv.h"
#include "plotter.h"
#include "photon.h"


class GainMedium {
public:
    void print();

    void set_units(double units);

    void set_gain_medium_data(GainMediumData gain_medium_data);

    virtual void make_mesh() {};

    virtual void preview(Plotter *plotter_) {};

    virtual void time_step(double dt, std::list<Photon> *photons_) {};

    virtual int is_inside(Coordinates coordinates_) {return -1;};

protected:
    GainMediumData data;
    double units;
    std::vector<std::vector<Dv *>> mesh;

};

class Rectangle : public GainMedium {
public:
    Rectangle(GainMediumData gainm_edium_data_,
              long center_x_,
              long center_y_,
              unsigned long dimension_x_,
              unsigned long dimension_y_);

    Rectangle(double n_,
              double tau_,
              unsigned int pump_rate_,
              double transition_cross_section_,
              double emission_frequency_,
              long center_x_,
              long center_y_,
              unsigned long dimension_x_,
              unsigned long dimension_y_);

    void preview(Plotter *plotter_) override;
    void make_mesh() override;
    int is_inside(Coordinates coordinates_) override;

private:
    long center_x;
    long center_y;
    unsigned long dimension_x;
    unsigned long dimension_y;
};

#endif // GAIN_MEDIUM_H