#ifndef AUX_H
#define AUX_H

#define C_0 299792458

typedef struct
{
    double x;
    double y;
} Coordinates;

typedef struct {
    double n;
    double tau;
    unsigned int pump_rate;
    double transition_cross_section;
    double emission_frequency;
} GainMediumData;

#endif // AUX_H