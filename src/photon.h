#ifndef PHOTON_H
#define PHOTON_H

#include "aux.h"


class Photon
{
    public:
        Photon(Coordinates location,
               double direction,
               double phase,
               double frequency);
        void update(double dt);
        Coordinates get_location(void);
        void set_c(double n);

    
    private:
        Coordinates location;
        double direction;
        double phase;
        double frequency;
        double c;
};

#endif // PHOTON_H