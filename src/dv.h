#ifndef DV_H
#define DV_H

#include <vector>
#include <list>
#include <random>
#include "photon.h"
#include "aux.h"

class Dv
{
    public:
        explicit Dv(GainMediumData *gain_medium_data_);
        void time_step(double dt, Coordinates coordinates_, std::vector<Photon> *photons_in_cell_, std::list<Photon> *photons_);

    private:
        GainMediumData *gain_medium_data;
        unsigned long N2;
        double N2ostanek;
        std::random_device direction_rd;
        std::uniform_real_distribution<double> direction_distribution;
        std::default_random_engine direction_generator;
        std::random_device phase_rd;
        std::uniform_real_distribution<double> phase_distribution;
        std::default_random_engine phase_generator;
        void time_step_pump();
        void time_step_spont_emiss(Coordinates coordinates_, std::list<Photon> *photons);
        void time_step_stimul_emiss(std::vector<Photon> *photons);
};

#endif // DV_H