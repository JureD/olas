#include <iostream>
#include <gtkmm.h>

int main(int argc, char* argv[]) {
    auto application = Gtk::Application::create("com.gitlab.JureD.olas");

    application->register_application();

    Gtk::Window window;
    window.set_title("OpenLAserSimulator");
    window.set_default_size(800, 300);

    Gtk::DrawingArea simulation_area;
//    window.add(simulation_area);
//    simulation_area.show();

    application->add_window(window);

    return application->make_window_and_run<Gtk::Window>(argc, argv);
}