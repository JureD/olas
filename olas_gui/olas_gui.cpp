#include <Magick++.h>
#include <iostream>



class Plotter {
private:
    unsigned long x_dimension;
    unsigned long y_dimension;
    Magick::Image *image;
    Magick::Pixels *view;
    Magick::Quantum *pixels;
public:
        Plotter(unsigned long x_dimension,unsigned long y_dimension)
    {
        x_dimension = x_dimension;
        y_dimension = y_dimension;

        Magick::InitializeMagick("");

        image = new Magick::Image(Magick::Geometry(x_dimension, y_dimension), "white");

        image->type(Magick::TrueColorType);
        image->modifyImage();

        view = new Magick::Pixels(*image);

        pixels = view->get(0, 0, x_dimension, y_dimension);

    };
    void draw_photon(unsigned long x, unsigned long y)
    {
        const Magick::Color photon_color(1,0,0);

        paint_pixel(x, y, photon_color);

        view->sync();

    };

    void paint_pixel(unsigned int x, unsigned int y, Magick::Color color)
    {
        *(pixels+(y*x_dimension + x)*3) = QuantumRange * color.quantumRed();
        *(pixels+(y*x_dimension + x)*3+1) = QuantumRange * color.quantumGreen();
        *(pixels+(y*x_dimension + x)*3+2) = QuantumRange * color.quantumBlue();
    }

    void save(std::string filename)
    {
        image->write(filename);
    }
};


int main() {
    Plotter plotter(30,20);

    plotter.draw_photon(0,0);

    plotter.save("test.png");
}























//using namespace std;
//using namespace Magick;
//int main(int argc,char **argv) {
//
//    unsigned long x_dimension = 20;
//    unsigned long y_dimension = 10;
//    unsigned long x = 1;
//    unsigned long y = 9;
//
//    InitializeMagick(*argv);
//
//    // Create base image
//    Image image(Geometry(x_dimension, y_dimension), "white");
//
//
//    // Set the image type to TrueColor DirectClass representation.
//    image.type(TrueColorType);
//    // Ensure that there is only one reference to underlying image
//    // If this is not done, then image pixels will not be modified.
//    image.modifyImage();
//
//    // Allocate pixel view
//    Pixels view(image);
//
//    // Set all pixels in region anchored at 38x36, with size 160x230 to green.
//    Color color(1,0,0);
//    Quantum *pixels = view.get(0, 0, x_dimension, y_dimension);
//
//    *(pixels+(y*x_dimension + x)*3) = QuantumRange * color.quantumRed();
//    *(pixels+(y*x_dimension + x)*3+1) = QuantumRange * color.quantumGreen();
//    *(pixels+(y*x_dimension + x)*3+2) = QuantumRange * color.quantumBlue();
////    pixels += (9*10+9)*3;
////    *pixels++ = QuantumRange * green.quantumRed();
////    *pixels++ = QuantumRange * green.quantumGreen();
////    *pixels = QuantumRange * green.quantumBlue();
//
//
//
//
//    // Save changes to image.
//    view.sync();
//    image.write( "logo.png" );
//}
