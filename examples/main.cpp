#include <iostream>

#include "../src/simulation.h"
#include "../src/resonator.h"
#include "../src/gain_medium.h"
#include "../src/gain_medium_shape.h"

int main(int argc, char* argv[])
{
    const auto x_dimension = static_cast<unsigned long>(30);
    const auto y_dimension = static_cast<unsigned long>(20);
    Simulation simulation(x_dimension, y_dimension, -3, 3, 1);

    // Create gain medium shape
    Rectangle rectangle(15,10,5,5);

    // Create gain medium object
    GainMedium gain_medium(1.76, 4.8e-9, 3, 2.5e-24, 400e12);

    // Add gain medium shape to object
    gain_medium.set_shape(&rectangle);

    simulation.resonator->add_gain_medium(&gain_medium);


    simulation.preview();






    return 0;
}